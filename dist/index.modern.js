import React__default, { useMemo, useState, useEffect, createElement, Fragment } from 'react';
import { setCookie, getCookie } from 'react-use-cookie';
import create from 'zustand';
import { useForm, useFormContext, FormProvider } from 'react-hook-form';
import { uid } from 'react-uid';

const cookieStore = create(set => ({
  activeCookies: undefined,
  editCookie: undefined,
  optionsVisible: false,
  dialogVisible: false,
  newSettings: undefined,
  config: undefined,
  message: undefined,
  setConfig: config => set(() => ({
    config
  })),
  setActiveCookies: cookies => set(() => ({
    activeCookies: cookies
  })),
  setEditCookie: handle => set(() => ({
    editCookie: handle
  })),
  setOptionsVisible: visible => set(() => ({
    optionsVisible: visible
  })),
  setDialogVisible: visible => set(() => ({
    dialogVisible: visible
  })),
  setNewSettings: setting => set(() => ({
    newSettings: setting
  })),
  setMessage: setting => set(() => ({
    message: setting
  }))
}));

const useCookies = () => {
  const {
    setMessage,
    setEditCookie,
    setDialogVisible,
    activeCookies,
    config
  } = cookieStore();

  const checkConsent = handle => {
    if (!activeCookies) {
      return false;
    }

    return activeCookies[handle] || false;
  };

  return {
    config,
    setMessage,
    setEditCookie,
    setDialogVisible,
    activeCookies,
    checkConsent
  };
};

const ConsentWrapper = props => {
  const {
    checkConsent,
    activeCookies
  } = useCookies();
  const cookieAccepted = useMemo(() => checkConsent(props.handle), [activeCookies, props.handle]);
  return React__default.createElement(React__default.Fragment, null, cookieAccepted ? props.consent : props.default);
};

const saveCookie = (settings, name) => {
  const payload = JSON.stringify(settings);
  setCookie(name || 'MECC_TECHNICAL', payload, {
    path: '/',
    days: 365
  });
};

var styles = {"test":"_me-react-cookie-consent-module__test__1lfnL","cookie-banner":"_me-react-cookie-consent-module__cookie-banner__1DhL0","overlay":"_me-react-cookie-consent-module__overlay__1rKzv","cookie-option":"_me-react-cookie-consent-module__cookie-option__3wQqS","cookie-option__checkbox":"_me-react-cookie-consent-module__cookie-option__checkbox__3HndL","close-button":"_me-react-cookie-consent-module__close-button__2I1sS","cookie-banner__buttons":"_me-react-cookie-consent-module__cookie-banner__buttons__18PE6","default-button":"_me-react-cookie-consent-module__default-button__3Cox5","content":"_me-react-cookie-consent-module__content__o5N5X","options":"_me-react-cookie-consent-module__options__3uRTj","option-group__header":"_me-react-cookie-consent-module__option-group__header__3XaPZ","option-group__toggler":"_me-react-cookie-consent-module__option-group__toggler__1Nra7","label":"_me-react-cookie-consent-module__label__3sron","option-group__toggler--hidden":"_me-react-cookie-consent-module__option-group__toggler--hidden__1RnZU","input":"_me-react-cookie-consent-module__input__3XhQY","option-group__toggler--intermediate":"_me-react-cookie-consent-module__option-group__toggler--intermediate__HDYv-","footer":"_me-react-cookie-consent-module__footer__eUfKy"};

const NewSettingsForm = props => {
  var _props$banner, _props$banner$buttons, _props$banner$buttons2, _props$banner2, _props$banner2$button, _props$banner2$button2;

  const methods = useForm();
  const {
    newSettings,
    activeCookies,
    setActiveCookies,
    config,
    setDialogVisible,
    setOptionsVisible,
    setNewSettings,
    setMessage
  } = cookieStore();

  const onFormSubmit = values => {
    const updateCookie = { ...activeCookies,
      ...values
    };
    saveCookie(updateCookie, (config === null || config === void 0 ? void 0 : config.cookieName) || '');
    setActiveCookies(updateCookie);
    setDialogVisible(false);
    setOptionsVisible(false);
    setNewSettings(null);
    setMessage(null);
  };

  const getNewSettings = cookies => {
    const settings = cookies.map((v, i) => {
      const key = `additional-cookie-checkbox--${i}`;

      if (!v) {
        return React__default.createElement(React__default.Fragment, null);
      }

      return React__default.createElement("div", {
        className: styles['cookie-option'],
        key: key
      }, React__default.createElement("input", Object.assign({
        type: 'checkbox',
        className: styles['cookie-option__checkbox'],
        id: key
      }, methods.register(v.handle))), React__default.createElement("label", {
        className: styles.label,
        htmlFor: key
      }, v.label));
    });
    return settings;
  };

  return React__default.createElement("form", {
    className: styles['option-group'],
    onSubmit: methods.handleSubmit(onFormSubmit)
  }, newSettings && getNewSettings(newSettings), React__default.createElement("button", {
    className: ((_props$banner = props.banner) === null || _props$banner === void 0 ? void 0 : (_props$banner$buttons = _props$banner.buttons) === null || _props$banner$buttons === void 0 ? void 0 : (_props$banner$buttons2 = _props$banner$buttons.acceptSettings) === null || _props$banner$buttons2 === void 0 ? void 0 : _props$banner$buttons2.classes) || styles['default-button'],
    type: 'submit'
  }, ((_props$banner2 = props.banner) === null || _props$banner2 === void 0 ? void 0 : (_props$banner2$button = _props$banner2.buttons) === null || _props$banner2$button === void 0 ? void 0 : (_props$banner2$button2 = _props$banner2$button.acceptSettings) === null || _props$banner2$button2 === void 0 ? void 0 : _props$banner2$button2.label) || 'Confirm Settings'));
};

const Options = ({
  options,
  mandatory: _mandatory = false
}) => {
  const {
    register
  } = useFormContext();
  const {
    activeCookies
  } = cookieStore();
  return React__default.createElement(React__default.Fragment, null, options.map((v, i) => {
    const savedValue = activeCookies ? activeCookies[v.handle] : undefined;
    const isChecked = savedValue !== undefined ? savedValue : _mandatory;
    return React__default.createElement("label", {
      key: `co-${i}-${v.handle}`,
      className: `${styles['cookie-option']} ${styles.label}`
    }, React__default.createElement("input", Object.assign({
      className: styles['cookie-option__checkbox'],
      type: 'checkbox',
      defaultChecked: isChecked,
      defaultValue: isChecked ? 1 : 0
    }, register(`${_mandatory ? 'mandatory.' : 'optional.'}${v.handle}`))), React__default.createElement("div", {
      className: styles['cookie-option__description']
    }, v.label));
  }));
};

const OptionsSingleGroup = ({
  group,
  mandatory,
  mandatoryLabel: _mandatoryLabel = 'mandatory',
  hasSelectedOptions,
  hasUnselectedOptions,
  index
}) => {
  var _config$banner, _config$banner2, _config$banner3, _config$banner4, _config$banner5;

  const [isDirty, setIsDirty] = useState(false);
  const {
    setValue
  } = useFormContext();
  const {
    config
  } = cookieStore();

  const toggleChildOptions = (options, value) => {
    options.forEach(option => {
      setValue('optional.' + option.handle, value);
    });
  };

  const groupHasIntermediateStatus = hasSelectedOptions && hasUnselectedOptions;
  const groupHasCheckedStatus = hasSelectedOptions && !hasUnselectedOptions || mandatory;
  const key = `option-group-control-${mandatory ? '-mandatory-' : ''}${index}`;
  return React__default.createElement("div", {
    className: `${styles['option-group']} ${config === null || config === void 0 ? void 0 : (_config$banner = config.banner) === null || _config$banner === void 0 ? void 0 : _config$banner.cOptionGroup}`,
    key: key
  }, React__default.createElement("div", {
    className: `${styles['option-group__header']} ${config === null || config === void 0 ? void 0 : (_config$banner2 = config.banner) === null || _config$banner2 === void 0 ? void 0 : _config$banner2.cOptionGroupHeader}`
  }, React__default.createElement("div", {
    className: `${styles[`option-group__toggler`]}
          ${config !== null && config !== void 0 && (_config$banner3 = config.banner) !== null && _config$banner3 !== void 0 && _config$banner3.cOptionGroupToggler ? config.banner.cOptionGroupToggler : ''} ${groupHasIntermediateStatus && !isDirty ? styles['option-group__toggler--intermediate'] : ''} ${mandatory ? styles['option-group__toggler--hidden'] : ''}`
  }, React__default.createElement("input", {
    className: styles.input,
    name: key,
    type: 'checkbox',
    id: key,
    defaultChecked: groupHasCheckedStatus,
    defaultValue: groupHasCheckedStatus ? 1 : 0,
    disabled: !!mandatory,
    onChange: data => {
      setIsDirty(true);
      toggleChildOptions(group.options, data.target.checked ? 1 : 0);
    }
  }), React__default.createElement("label", {
    className: styles.label,
    htmlFor: key
  }), mandatory && React__default.createElement("div", {
    className: `mandatory-label      ${config !== null && config !== void 0 && (_config$banner4 = config.banner) !== null && _config$banner4 !== void 0 && _config$banner4.cOptionGroupTogglerMandatory ? config === null || config === void 0 ? void 0 : (_config$banner5 = config.banner) === null || _config$banner5 === void 0 ? void 0 : _config$banner5.cOptionGroupTogglerMandatory : ''}`
  }, React__default.createElement("small", null, _mandatoryLabel))), React__default.createElement("div", {
    className: 'option-group__description'
  }, group.headline, group.description)), React__default.createElement("div", {
    className: styles['option-group__options']
  }, React__default.createElement("fieldset", {
    disabled: true
  }, React__default.createElement(Options, {
    options: group.options,
    mandatory: mandatory
  }))));
};

const OptionsGroups = ({
  groups,
  mandatory,
  mandatoryLabel: _mandatoryLabel = 'mandatory'
}) => {
  const {
    activeCookies
  } = cookieStore();

  const checkIfGroupPreselected = group => {
    const hasSelected = group.some(({
      handle
    }) => activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle]);
    const hasUnselected = group.some(({
      handle
    }) => (activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle]) === undefined || (activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle]) === false);
    return {
      hasSelected,
      hasUnselected
    };
  };

  return React__default.createElement(React__default.Fragment, null, groups.map((group, i) => {
    const {
      hasSelected,
      hasUnselected
    } = checkIfGroupPreselected(group.options);
    return React__default.createElement(OptionsSingleGroup, {
      group: group,
      mandatory: mandatory,
      mandatoryLabel: _mandatoryLabel,
      hasSelectedOptions: hasSelected,
      hasUnselectedOptions: hasUnselected,
      index: i
    });
  }));
};

const CookieOptionsForm = props => {
  var _buttons$acceptSettin, _buttons$acceptSettin2, _buttons$closeSetting, _buttons$closeSetting2;

  const {
    cookies,
    onDecline,
    onAbort,
    banner: {
      buttons,
      cOptionsForm = undefined
    }
  } = props;
  const methods = useForm();
  const {
    config,
    editCookie,
    newSettings,
    setDialogVisible,
    setActiveCookies,
    setEditCookie,
    message,
    setMessage
  } = cookieStore();

  const onFormSubmit = (cookieValues = {}) => {
    const optionalCookieValues = { ...cookieValues.optional
    };
    const updateValues = {};
    Object.keys(optionalCookieValues).forEach(key => updateValues[key] = !!optionalCookieValues[key] ? true : false);
    saveCookie(updateValues, (config === null || config === void 0 ? void 0 : config.cookieName) || '');
    setActiveCookies && setActiveCookies(updateValues);
    props.onDialogSubmit && props.onDialogSubmit(updateValues);
    setDialogVisible(false);
    setEditCookie(null);
    setMessage(null);
  };

  const getMessage = message => {
    return React__default.createElement("div", {
      className: styles['message']
    }, message);
  };

  return React__default.createElement(FormProvider, Object.assign({}, methods), message && getMessage(message || ''), newSettings ? React__default.createElement(React__default.Fragment, null, getMessage((config === null || config === void 0 ? void 0 : config.banner.newCookieFoundMessage) || ''), React__default.createElement(NewSettingsForm, {
    banner: props.banner
  })) : React__default.createElement("form", {
    onSubmit: methods.handleSubmit(onFormSubmit),
    className: `${styles.options} ${cOptionsForm ? cOptionsForm : ''}`
  }, React__default.createElement(OptionsGroups, {
    groups: cookies.mandatory,
    mandatory: true,
    mandatoryLabel: config === null || config === void 0 ? void 0 : config.banner.mandatoryLabel
  }), React__default.createElement(OptionsGroups, {
    groups: cookies.optional,
    mandatory: false
  }), React__default.createElement("button", {
    className: buttons === null || buttons === void 0 ? void 0 : (_buttons$acceptSettin = buttons.acceptSettings) === null || _buttons$acceptSettin === void 0 ? void 0 : _buttons$acceptSettin.classes,
    type: 'submit'
  }, (buttons === null || buttons === void 0 ? void 0 : (_buttons$acceptSettin2 = buttons.acceptSettings) === null || _buttons$acceptSettin2 === void 0 ? void 0 : _buttons$acceptSettin2.label) || 'Confirm Settings'), React__default.createElement("button", {
    className: buttons === null || buttons === void 0 ? void 0 : (_buttons$closeSetting = buttons.closeSettings) === null || _buttons$closeSetting === void 0 ? void 0 : _buttons$closeSetting.classes,
    onClick: editCookie ? onDecline : onAbort
  }, (buttons === null || buttons === void 0 ? void 0 : (_buttons$closeSetting2 = buttons.closeSettings) === null || _buttons$closeSetting2 === void 0 ? void 0 : _buttons$closeSetting2.label) || 'Cancel')));
};

const getDeactivatedCookieList = config => {
  const cookieSettings = {};
  config.cookies.optional.map(group => group.options).flat().map(({
    handle
  }) => cookieSettings[handle] = false);
  return cookieSettings;
};

const useActivateService = () => {
  const {
    config,
    activeCookies,
    setActiveCookies
  } = cookieStore();

  const activate = cookieHandle => {
    if (!(config !== null && config !== void 0 && config.cookieName)) {
      console.log('error: cannot activate service - no cookiename provided');
      return;
    }

    const servicesList = activeCookies ? activeCookies : getDeactivatedCookieList(config);
    const services = { ...servicesList,
      [cookieHandle]: true
    };
    saveCookie(services, config.cookieName);
    setActiveCookies(services);
  };

  return activate;
};

var style = {"table":"_table-module__table__1nkyE","head":"_table-module__head__2ybDW","body":"_table-module__body__28THJ"};

const CookieTable = ({
  cookies: {
    optional,
    mandatory
  },
  onlyMandatory: _onlyMandatory = false,
  onlyOptional: _onlyOptional = false,
  labelCookieDuration: _labelCookieDuration = 'Valid for',
  labelCookieDescription: _labelCookieDescription = 'Description',
  labelCookieName: _labelCookieName = 'Name',
  ...props
}) => {
  const tableHeader = React__default.createElement("thead", {
    className: `${style.head} ${props.cHead}`
  }, React__default.createElement("tr", null, React__default.createElement("td", null, _labelCookieName), React__default.createElement("td", null, _labelCookieDescription), React__default.createElement("td", null, _labelCookieDuration)));

  const renderRow = cookies => {
    return React__default.createElement(React__default.Fragment, {
      key: uid(cookies)
    }, cookies.map(({
      duration: _duration = '',
      name: _name = '',
      description: _description = ''
    }, i) => React__default.createElement("tr", {
      key: uid(i)
    }, React__default.createElement("td", null, _name), React__default.createElement("td", null, _description), React__default.createElement("td", null, _duration))));
  };

  const renderTable = optionGroups => {
    return React__default.createElement("table", {
      className: `${style.table} ${props.cTable}`
    }, tableHeader, React__default.createElement("tbody", {
      className: `${style.body} ${props.cBody}`
    }, optionGroups.map(({
      options
    }) => renderRow(options))));
  };

  const filterOptions = (optionGroups, handles) => {
    return optionGroups.filter(v => handles.includes(v.handle || ''));
  };

  return React__default.createElement(React__default.Fragment, null, !_onlyOptional && renderTable(mandatory), !_onlyMandatory && renderTable(props.groupHandles ? filterOptions(optional, props.groupHandles) : optional));
};

const CookieDialog = ({
  overlay: _overlay = false,
  config
}) => {
  var _config$banner, _config$banner$button, _config$banner$button2, _banner$buttons, _banner$buttons$close;

  const {
    editCookie,
    dialogVisible,
    optionsVisible,
    setNewSettings,
    setConfig,
    setDialogVisible,
    setActiveCookies,
    setEditCookie,
    setOptionsVisible
  } = cookieStore();
  const {
    activeCookies
  } = useCookies();

  const checkConsentStatus = () => {
    const cookieJSON = getCookie(config.cookieName);

    if (getCookie(config.cookieName)) {
      const cookie = JSON.parse(cookieJSON);
      const newSettingsFound = checkForNewSettings(config.cookies, cookie);

      if (newSettingsFound.length > 0) {
        setNewSettings(newSettingsFound);
        setDialogVisible(true);
        setOptionsVisible(true);
      }

      setActiveCookies(cookie);
    } else {
      setTimeout(() => {
        setDialogVisible(true);
      }, config.banner.delay || 0);
    }
  };

  const onHashChange = () => {
    const {
      openSettingsHash
    } = config;

    if (openSettingsHash && openSettingsHash === location.hash.substring(1)) {
      history.replaceState({}, document.title, window.location.href.split('#')[0]);
      setDialogVisible(true);
      setOptionsVisible(true);
    }
  };

  useEffect(() => {
    if (!activeCookies) {
      return;
    }

    Object.keys(activeCookies).forEach(key => {
      if (activeCookies[key] === false) {
        return;
      }

      const script = document.querySelector(`script[data-service-handle="${key}"]`);

      if (!script || script.type !== 'text/plain') {
        return;
      }

      let updatedScript = script.cloneNode(true);

      if (updatedScript && script) {
        var _script$parentNode;

        updatedScript.type = 'text/javascript';
        (_script$parentNode = script.parentNode) === null || _script$parentNode === void 0 ? void 0 : _script$parentNode.replaceChild(updatedScript, script);
      }
    });
  }, [activeCookies]);
  useEffect(() => {
    setConfig(config);
    checkConsentStatus();
    window.addEventListener('hashchange', () => {
      onHashChange();
    });
    return window.removeEventListener('hashchange', onHashChange);
  }, []);

  const checkForNewSettings = (cookieConfig, cookie) => {
    const cookieList = cookieConfig.optional.map(group => group.options).flat();
    return cookieList.map(v => {
      if (v.handle in cookie) {
        return null;
      } else {
        return v;
      }
    }).filter(v => v);
  };

  useEffect(() => {
    if (!editCookie) {
      return;
    }

    setDialogVisible(true);
    setOptionsVisible(true);
  }, [editCookie]);

  const onAccept = () => {
    const cookieSettings = {};
    const cookiesList = config.cookies.optional.map(group => group.options).flat();
    cookiesList.map(({
      handle
    }) => cookieSettings[handle] = true);
    saveCookie(cookieSettings, config.cookieName);
    setActiveCookies(cookieSettings);
    setDialogVisible(false);
    setEditCookie(null);
  };

  const onDecline = () => {
    const cookieSettings = getDeactivatedCookieList(config);
    saveCookie(cookieSettings, config.cookieName);
    setDialogVisible(false);
    setActiveCookies(cookieSettings);
    setEditCookie(null);
  };

  const onAbort = () => {
    setDialogVisible(false);
    setEditCookie(null);
  };

  if (!dialogVisible) {
    return null;
  }

  const buttons = () => {
    var _buttonProps$openSett, _buttonProps$openSett2, _buttonProps$decline, _buttonProps$decline2, _buttonProps$accept, _buttonProps$accept2;

    const buttonProps = config.banner.buttons;
    return createElement("div", {
      className: `${styles['cookie-banner__buttons']} ${config.banner.cButtonWrapper}`
    }, createElement("button", {
      className: `
            ${styles.button}

            ${(buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$openSett = buttonProps.openSettings) === null || _buttonProps$openSett === void 0 ? void 0 : _buttonProps$openSett.classes) || styles['default-button']}`,
      onClick: () => {
        setOptionsVisible(true);
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$openSett2 = buttonProps.openSettings) === null || _buttonProps$openSett2 === void 0 ? void 0 : _buttonProps$openSett2.label), createElement("button", {
      className: `
          ${styles.button}

          ${(buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$decline = buttonProps.decline) === null || _buttonProps$decline === void 0 ? void 0 : _buttonProps$decline.classes) || styles['default-button']}`,
      onClick: () => {
        onDecline();
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$decline2 = buttonProps.decline) === null || _buttonProps$decline2 === void 0 ? void 0 : _buttonProps$decline2.label), createElement("button", {
      className: `
          ${styles.button}
          ${(buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$accept = buttonProps.accept) === null || _buttonProps$accept === void 0 ? void 0 : _buttonProps$accept.classes) || styles['default-button']}`,
      onClick: () => {
        onAccept();
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$accept2 = buttonProps.accept) === null || _buttonProps$accept2 === void 0 ? void 0 : _buttonProps$accept2.label));
  };

  const {
    banner
  } = config;
  const bannerJSX = createElement("div", {
    className: `
        ${styles['cookie-banner']}
        ${banner.className}
        ${optionsVisible && `${styles['cookie-banner--options-visible']} ${banner.optionModeClassName} `}
    `
  }, createElement("div", {
    className: `${styles.content} ${(_config$banner = config.banner) !== null && _config$banner !== void 0 && _config$banner.cContent ? config.banner.cContent : ''}`,
    style: {}
  }, optionsVisible ? createElement(CookieOptionsForm, {
    onAbort: onAbort,
    onDecline: onDecline,
    cookies: config.cookies,
    banner: banner
  }) : banner.content), !optionsVisible && buttons(), createElement("button", {
    className: `
          ${styles.button}

          ${((_config$banner$button = config.banner.buttons) === null || _config$banner$button === void 0 ? void 0 : (_config$banner$button2 = _config$banner$button.closeButton) === null || _config$banner$button2 === void 0 ? void 0 : _config$banner$button2.classes) || styles['close-button']}`,
    onClick: () => {
      setDialogVisible(false);
    }
  }, ((_banner$buttons = banner.buttons) === null || _banner$buttons === void 0 ? void 0 : (_banner$buttons$close = _banner$buttons.closeButton) === null || _banner$buttons$close === void 0 ? void 0 : _banner$buttons$close.label) || 'X'), banner.footer && !optionsVisible && createElement("div", {
    className: banner.cFfooter || styles['footer']
  }, banner.footer));

  if (_overlay) {
    return createElement("div", {
      className: `
          ${config.banner.cOverlay ? config.banner.cOverlay : ''}  ${styles['overlay']}
        `
    }, bannerJSX);
  }

  return createElement(Fragment, null, " ", bannerJSX, " ");
};

export { ConsentWrapper, CookieDialog, CookieTable, useActivateService, useCookies };
//# sourceMappingURL=index.modern.js.map
