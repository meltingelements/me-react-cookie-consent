import * as React from 'react';
import { ConsentWrapper } from './components/consent-wrapper';
import { useActivateService } from './helper/activateService';
import { useCookies } from './hooks/useCookies';
import { CookieTable } from './table';
import { CookieOption, ICookieBannerProps, ICookieConfig, ICookiesProps, MeCookieConsentProps, OptionGroup } from './types';
declare const CookieDialog: React.FunctionComponent<MeCookieConsentProps>;
export { ConsentWrapper, CookieDialog, CookieTable, CookieOption, useCookies, MeCookieConsentProps, ICookieBannerProps, ICookieConfig, OptionGroup, ICookiesProps, useActivateService };
