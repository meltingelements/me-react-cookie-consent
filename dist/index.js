function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var reactUseCookie = require('react-use-cookie');
var create = _interopDefault(require('zustand'));
var reactHookForm = require('react-hook-form');
var reactUid = require('react-uid');

var cookieStore = create(function (set) {
  return {
    activeCookies: undefined,
    editCookie: undefined,
    optionsVisible: false,
    dialogVisible: false,
    newSettings: undefined,
    config: undefined,
    message: undefined,
    setConfig: function setConfig(config) {
      return set(function () {
        return {
          config: config
        };
      });
    },
    setActiveCookies: function setActiveCookies(cookies) {
      return set(function () {
        return {
          activeCookies: cookies
        };
      });
    },
    setEditCookie: function setEditCookie(handle) {
      return set(function () {
        return {
          editCookie: handle
        };
      });
    },
    setOptionsVisible: function setOptionsVisible(visible) {
      return set(function () {
        return {
          optionsVisible: visible
        };
      });
    },
    setDialogVisible: function setDialogVisible(visible) {
      return set(function () {
        return {
          dialogVisible: visible
        };
      });
    },
    setNewSettings: function setNewSettings(setting) {
      return set(function () {
        return {
          newSettings: setting
        };
      });
    },
    setMessage: function setMessage(setting) {
      return set(function () {
        return {
          message: setting
        };
      });
    }
  };
});

var useCookies = function useCookies() {
  var _cookieStore = cookieStore(),
      setMessage = _cookieStore.setMessage,
      setEditCookie = _cookieStore.setEditCookie,
      setDialogVisible = _cookieStore.setDialogVisible,
      activeCookies = _cookieStore.activeCookies,
      config = _cookieStore.config;

  var checkConsent = function checkConsent(handle) {
    if (!activeCookies) {
      return false;
    }

    return activeCookies[handle] || false;
  };

  return {
    config: config,
    setMessage: setMessage,
    setEditCookie: setEditCookie,
    setDialogVisible: setDialogVisible,
    activeCookies: activeCookies,
    checkConsent: checkConsent
  };
};

var ConsentWrapper = function ConsentWrapper(props) {
  var _useCookies = useCookies(),
      checkConsent = _useCookies.checkConsent,
      activeCookies = _useCookies.activeCookies;

  var cookieAccepted = React.useMemo(function () {
    return checkConsent(props.handle);
  }, [activeCookies, props.handle]);
  return React__default.createElement(React__default.Fragment, null, cookieAccepted ? props.consent : props["default"]);
};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var saveCookie = function saveCookie(settings, name) {
  var payload = JSON.stringify(settings);
  reactUseCookie.setCookie(name || 'MECC_TECHNICAL', payload, {
    path: '/',
    days: 365
  });
};

var styles = {"test":"_me-react-cookie-consent-module__test__1lfnL","cookie-banner":"_me-react-cookie-consent-module__cookie-banner__1DhL0","overlay":"_me-react-cookie-consent-module__overlay__1rKzv","cookie-option":"_me-react-cookie-consent-module__cookie-option__3wQqS","cookie-option__checkbox":"_me-react-cookie-consent-module__cookie-option__checkbox__3HndL","close-button":"_me-react-cookie-consent-module__close-button__2I1sS","cookie-banner__buttons":"_me-react-cookie-consent-module__cookie-banner__buttons__18PE6","default-button":"_me-react-cookie-consent-module__default-button__3Cox5","content":"_me-react-cookie-consent-module__content__o5N5X","options":"_me-react-cookie-consent-module__options__3uRTj","option-group__header":"_me-react-cookie-consent-module__option-group__header__3XaPZ","option-group__toggler":"_me-react-cookie-consent-module__option-group__toggler__1Nra7","label":"_me-react-cookie-consent-module__label__3sron","option-group__toggler--hidden":"_me-react-cookie-consent-module__option-group__toggler--hidden__1RnZU","input":"_me-react-cookie-consent-module__input__3XhQY","option-group__toggler--intermediate":"_me-react-cookie-consent-module__option-group__toggler--intermediate__HDYv-","footer":"_me-react-cookie-consent-module__footer__eUfKy"};

var NewSettingsForm = function NewSettingsForm(props) {
  var _props$banner, _props$banner$buttons, _props$banner$buttons2, _props$banner2, _props$banner2$button, _props$banner2$button2;

  var methods = reactHookForm.useForm();

  var _cookieStore = cookieStore(),
      newSettings = _cookieStore.newSettings,
      activeCookies = _cookieStore.activeCookies,
      setActiveCookies = _cookieStore.setActiveCookies,
      config = _cookieStore.config,
      setDialogVisible = _cookieStore.setDialogVisible,
      setOptionsVisible = _cookieStore.setOptionsVisible,
      setNewSettings = _cookieStore.setNewSettings,
      setMessage = _cookieStore.setMessage;

  var onFormSubmit = function onFormSubmit(values) {
    var updateCookie = _extends({}, activeCookies, values);

    saveCookie(updateCookie, (config === null || config === void 0 ? void 0 : config.cookieName) || '');
    setActiveCookies(updateCookie);
    setDialogVisible(false);
    setOptionsVisible(false);
    setNewSettings(null);
    setMessage(null);
  };

  var getNewSettings = function getNewSettings(cookies) {
    var settings = cookies.map(function (v, i) {
      var key = "additional-cookie-checkbox--" + i;

      if (!v) {
        return React__default.createElement(React__default.Fragment, null);
      }

      return React__default.createElement("div", {
        className: styles['cookie-option'],
        key: key
      }, React__default.createElement("input", Object.assign({
        type: 'checkbox',
        className: styles['cookie-option__checkbox'],
        id: key
      }, methods.register(v.handle))), React__default.createElement("label", {
        className: styles.label,
        htmlFor: key
      }, v.label));
    });
    return settings;
  };

  return React__default.createElement("form", {
    className: styles['option-group'],
    onSubmit: methods.handleSubmit(onFormSubmit)
  }, newSettings && getNewSettings(newSettings), React__default.createElement("button", {
    className: ((_props$banner = props.banner) === null || _props$banner === void 0 ? void 0 : (_props$banner$buttons = _props$banner.buttons) === null || _props$banner$buttons === void 0 ? void 0 : (_props$banner$buttons2 = _props$banner$buttons.acceptSettings) === null || _props$banner$buttons2 === void 0 ? void 0 : _props$banner$buttons2.classes) || styles['default-button'],
    type: 'submit'
  }, ((_props$banner2 = props.banner) === null || _props$banner2 === void 0 ? void 0 : (_props$banner2$button = _props$banner2.buttons) === null || _props$banner2$button === void 0 ? void 0 : (_props$banner2$button2 = _props$banner2$button.acceptSettings) === null || _props$banner2$button2 === void 0 ? void 0 : _props$banner2$button2.label) || 'Confirm Settings'));
};

var Options = function Options(_ref) {
  var options = _ref.options,
      _ref$mandatory = _ref.mandatory,
      mandatory = _ref$mandatory === void 0 ? false : _ref$mandatory;

  var _useFormContext = reactHookForm.useFormContext(),
      register = _useFormContext.register;

  var _cookieStore = cookieStore(),
      activeCookies = _cookieStore.activeCookies;

  return React__default.createElement(React__default.Fragment, null, options.map(function (v, i) {
    var savedValue = activeCookies ? activeCookies[v.handle] : undefined;
    var isChecked = savedValue !== undefined ? savedValue : mandatory;
    return React__default.createElement("label", {
      key: "co-" + i + "-" + v.handle,
      className: styles['cookie-option'] + " " + styles.label
    }, React__default.createElement("input", Object.assign({
      className: styles['cookie-option__checkbox'],
      type: 'checkbox',
      defaultChecked: isChecked,
      defaultValue: isChecked ? 1 : 0
    }, register("" + (mandatory ? 'mandatory.' : 'optional.') + v.handle))), React__default.createElement("div", {
      className: styles['cookie-option__description']
    }, v.label));
  }));
};

var OptionsSingleGroup = function OptionsSingleGroup(_ref) {
  var _config$banner, _config$banner2, _config$banner3, _config$banner4, _config$banner5;

  var group = _ref.group,
      mandatory = _ref.mandatory,
      _ref$mandatoryLabel = _ref.mandatoryLabel,
      mandatoryLabel = _ref$mandatoryLabel === void 0 ? 'mandatory' : _ref$mandatoryLabel,
      hasSelectedOptions = _ref.hasSelectedOptions,
      hasUnselectedOptions = _ref.hasUnselectedOptions,
      index = _ref.index;

  var _useState = React.useState(false),
      isDirty = _useState[0],
      setIsDirty = _useState[1];

  var _useFormContext = reactHookForm.useFormContext(),
      setValue = _useFormContext.setValue;

  var _cookieStore = cookieStore(),
      config = _cookieStore.config;

  var toggleChildOptions = function toggleChildOptions(options, value) {
    options.forEach(function (option) {
      setValue('optional.' + option.handle, value);
    });
  };

  var groupHasIntermediateStatus = hasSelectedOptions && hasUnselectedOptions;
  var groupHasCheckedStatus = hasSelectedOptions && !hasUnselectedOptions || mandatory;
  var key = "option-group-control-" + (mandatory ? '-mandatory-' : '') + index;
  return React__default.createElement("div", {
    className: styles['option-group'] + " " + (config === null || config === void 0 ? void 0 : (_config$banner = config.banner) === null || _config$banner === void 0 ? void 0 : _config$banner.cOptionGroup),
    key: key
  }, React__default.createElement("div", {
    className: styles['option-group__header'] + " " + (config === null || config === void 0 ? void 0 : (_config$banner2 = config.banner) === null || _config$banner2 === void 0 ? void 0 : _config$banner2.cOptionGroupHeader)
  }, React__default.createElement("div", {
    className: styles["option-group__toggler"] + "\n          " + (config !== null && config !== void 0 && (_config$banner3 = config.banner) !== null && _config$banner3 !== void 0 && _config$banner3.cOptionGroupToggler ? config.banner.cOptionGroupToggler : '') + " " + (groupHasIntermediateStatus && !isDirty ? styles['option-group__toggler--intermediate'] : '') + " " + (mandatory ? styles['option-group__toggler--hidden'] : '')
  }, React__default.createElement("input", {
    className: styles.input,
    name: key,
    type: 'checkbox',
    id: key,
    defaultChecked: groupHasCheckedStatus,
    defaultValue: groupHasCheckedStatus ? 1 : 0,
    disabled: !!mandatory,
    onChange: function onChange(data) {
      setIsDirty(true);
      toggleChildOptions(group.options, data.target.checked ? 1 : 0);
    }
  }), React__default.createElement("label", {
    className: styles.label,
    htmlFor: key
  }), mandatory && React__default.createElement("div", {
    className: "mandatory-label      " + (config !== null && config !== void 0 && (_config$banner4 = config.banner) !== null && _config$banner4 !== void 0 && _config$banner4.cOptionGroupTogglerMandatory ? config === null || config === void 0 ? void 0 : (_config$banner5 = config.banner) === null || _config$banner5 === void 0 ? void 0 : _config$banner5.cOptionGroupTogglerMandatory : '')
  }, React__default.createElement("small", null, mandatoryLabel))), React__default.createElement("div", {
    className: 'option-group__description'
  }, group.headline, group.description)), React__default.createElement("div", {
    className: styles['option-group__options']
  }, React__default.createElement("fieldset", {
    disabled: true
  }, React__default.createElement(Options, {
    options: group.options,
    mandatory: mandatory
  }))));
};

var OptionsGroups = function OptionsGroups(_ref) {
  var groups = _ref.groups,
      mandatory = _ref.mandatory,
      _ref$mandatoryLabel = _ref.mandatoryLabel,
      mandatoryLabel = _ref$mandatoryLabel === void 0 ? 'mandatory' : _ref$mandatoryLabel;

  var _cookieStore = cookieStore(),
      activeCookies = _cookieStore.activeCookies;

  var checkIfGroupPreselected = function checkIfGroupPreselected(group) {
    var hasSelected = group.some(function (_ref2) {
      var handle = _ref2.handle;
      return activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle];
    });
    var hasUnselected = group.some(function (_ref3) {
      var handle = _ref3.handle;
      return (activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle]) === undefined || (activeCookies === null || activeCookies === void 0 ? void 0 : activeCookies[handle]) === false;
    });
    return {
      hasSelected: hasSelected,
      hasUnselected: hasUnselected
    };
  };

  return React__default.createElement(React__default.Fragment, null, groups.map(function (group, i) {
    var _checkIfGroupPreselec = checkIfGroupPreselected(group.options),
        hasSelected = _checkIfGroupPreselec.hasSelected,
        hasUnselected = _checkIfGroupPreselec.hasUnselected;

    return React__default.createElement(OptionsSingleGroup, {
      group: group,
      mandatory: mandatory,
      mandatoryLabel: mandatoryLabel,
      hasSelectedOptions: hasSelected,
      hasUnselectedOptions: hasUnselected,
      index: i
    });
  }));
};

var CookieOptionsForm = function CookieOptionsForm(props) {
  var _buttons$acceptSettin, _buttons$acceptSettin2, _buttons$closeSetting, _buttons$closeSetting2;

  var cookies = props.cookies,
      onDecline = props.onDecline,
      onAbort = props.onAbort,
      _props$banner = props.banner,
      buttons = _props$banner.buttons,
      _props$banner$cOption = _props$banner.cOptionsForm,
      cOptionsForm = _props$banner$cOption === void 0 ? undefined : _props$banner$cOption;
  var methods = reactHookForm.useForm();

  var _cookieStore = cookieStore(),
      config = _cookieStore.config,
      editCookie = _cookieStore.editCookie,
      newSettings = _cookieStore.newSettings,
      setDialogVisible = _cookieStore.setDialogVisible,
      setActiveCookies = _cookieStore.setActiveCookies,
      setEditCookie = _cookieStore.setEditCookie,
      message = _cookieStore.message,
      setMessage = _cookieStore.setMessage;

  var onFormSubmit = function onFormSubmit(cookieValues) {
    if (cookieValues === void 0) {
      cookieValues = {};
    }

    var optionalCookieValues = _extends({}, cookieValues.optional);

    var updateValues = {};
    Object.keys(optionalCookieValues).forEach(function (key) {
      return updateValues[key] = !!optionalCookieValues[key] ? true : false;
    });
    saveCookie(updateValues, (config === null || config === void 0 ? void 0 : config.cookieName) || '');
    setActiveCookies && setActiveCookies(updateValues);
    props.onDialogSubmit && props.onDialogSubmit(updateValues);
    setDialogVisible(false);
    setEditCookie(null);
    setMessage(null);
  };

  var getMessage = function getMessage(message) {
    return React__default.createElement("div", {
      className: styles['message']
    }, message);
  };

  return React__default.createElement(reactHookForm.FormProvider, Object.assign({}, methods), message && getMessage(message || ''), newSettings ? React__default.createElement(React__default.Fragment, null, getMessage((config === null || config === void 0 ? void 0 : config.banner.newCookieFoundMessage) || ''), React__default.createElement(NewSettingsForm, {
    banner: props.banner
  })) : React__default.createElement("form", {
    onSubmit: methods.handleSubmit(onFormSubmit),
    className: styles.options + " " + (cOptionsForm ? cOptionsForm : '')
  }, React__default.createElement(OptionsGroups, {
    groups: cookies.mandatory,
    mandatory: true,
    mandatoryLabel: config === null || config === void 0 ? void 0 : config.banner.mandatoryLabel
  }), React__default.createElement(OptionsGroups, {
    groups: cookies.optional,
    mandatory: false
  }), React__default.createElement("button", {
    className: buttons === null || buttons === void 0 ? void 0 : (_buttons$acceptSettin = buttons.acceptSettings) === null || _buttons$acceptSettin === void 0 ? void 0 : _buttons$acceptSettin.classes,
    type: 'submit'
  }, (buttons === null || buttons === void 0 ? void 0 : (_buttons$acceptSettin2 = buttons.acceptSettings) === null || _buttons$acceptSettin2 === void 0 ? void 0 : _buttons$acceptSettin2.label) || 'Confirm Settings'), React__default.createElement("button", {
    className: buttons === null || buttons === void 0 ? void 0 : (_buttons$closeSetting = buttons.closeSettings) === null || _buttons$closeSetting === void 0 ? void 0 : _buttons$closeSetting.classes,
    onClick: editCookie ? onDecline : onAbort
  }, (buttons === null || buttons === void 0 ? void 0 : (_buttons$closeSetting2 = buttons.closeSettings) === null || _buttons$closeSetting2 === void 0 ? void 0 : _buttons$closeSetting2.label) || 'Cancel')));
};

var getDeactivatedCookieList = function getDeactivatedCookieList(config) {
  var cookieSettings = {};
  config.cookies.optional.map(function (group) {
    return group.options;
  }).flat().map(function (_ref) {
    var handle = _ref.handle;
    return cookieSettings[handle] = false;
  });
  return cookieSettings;
};

var useActivateService = function useActivateService() {
  var _cookieStore = cookieStore(),
      config = _cookieStore.config,
      activeCookies = _cookieStore.activeCookies,
      setActiveCookies = _cookieStore.setActiveCookies;

  var activate = function activate(cookieHandle) {
    var _extends2;

    if (!(config !== null && config !== void 0 && config.cookieName)) {
      console.log('error: cannot activate service - no cookiename provided');
      return;
    }

    var servicesList = activeCookies ? activeCookies : getDeactivatedCookieList(config);

    var services = _extends({}, servicesList, (_extends2 = {}, _extends2[cookieHandle] = true, _extends2));

    saveCookie(services, config.cookieName);
    setActiveCookies(services);
  };

  return activate;
};

var style = {"table":"_table-module__table__1nkyE","head":"_table-module__head__2ybDW","body":"_table-module__body__28THJ"};

var _excluded = ["cookies", "onlyMandatory", "onlyOptional", "labelCookieDuration", "labelCookieDescription", "labelCookieName"];
var CookieTable = function CookieTable(_ref) {
  var _ref$cookies = _ref.cookies,
      optional = _ref$cookies.optional,
      mandatory = _ref$cookies.mandatory,
      _ref$onlyMandatory = _ref.onlyMandatory,
      onlyMandatory = _ref$onlyMandatory === void 0 ? false : _ref$onlyMandatory,
      _ref$onlyOptional = _ref.onlyOptional,
      onlyOptional = _ref$onlyOptional === void 0 ? false : _ref$onlyOptional,
      _ref$labelCookieDurat = _ref.labelCookieDuration,
      labelCookieDuration = _ref$labelCookieDurat === void 0 ? 'Valid for' : _ref$labelCookieDurat,
      _ref$labelCookieDescr = _ref.labelCookieDescription,
      labelCookieDescription = _ref$labelCookieDescr === void 0 ? 'Description' : _ref$labelCookieDescr,
      _ref$labelCookieName = _ref.labelCookieName,
      labelCookieName = _ref$labelCookieName === void 0 ? 'Name' : _ref$labelCookieName,
      props = _objectWithoutPropertiesLoose(_ref, _excluded);

  var tableHeader = React__default.createElement("thead", {
    className: style.head + " " + props.cHead
  }, React__default.createElement("tr", null, React__default.createElement("td", null, labelCookieName), React__default.createElement("td", null, labelCookieDescription), React__default.createElement("td", null, labelCookieDuration)));

  var renderRow = function renderRow(cookies) {
    return React__default.createElement(React__default.Fragment, {
      key: reactUid.uid(cookies)
    }, cookies.map(function (_ref2, i) {
      var _ref2$duration = _ref2.duration,
          duration = _ref2$duration === void 0 ? '' : _ref2$duration,
          _ref2$name = _ref2.name,
          name = _ref2$name === void 0 ? '' : _ref2$name,
          _ref2$description = _ref2.description,
          description = _ref2$description === void 0 ? '' : _ref2$description;
      return React__default.createElement("tr", {
        key: reactUid.uid(i)
      }, React__default.createElement("td", null, name), React__default.createElement("td", null, description), React__default.createElement("td", null, duration));
    }));
  };

  var renderTable = function renderTable(optionGroups) {
    return React__default.createElement("table", {
      className: style.table + " " + props.cTable
    }, tableHeader, React__default.createElement("tbody", {
      className: style.body + " " + props.cBody
    }, optionGroups.map(function (_ref3) {
      var options = _ref3.options;
      return renderRow(options);
    })));
  };

  var filterOptions = function filterOptions(optionGroups, handles) {
    return optionGroups.filter(function (v) {
      return handles.includes(v.handle || '');
    });
  };

  return React__default.createElement(React__default.Fragment, null, !onlyOptional && renderTable(mandatory), !onlyMandatory && renderTable(props.groupHandles ? filterOptions(optional, props.groupHandles) : optional));
};

var CookieDialog = function CookieDialog(_ref) {
  var _config$banner, _config$banner$button, _config$banner$button2, _banner$buttons, _banner$buttons$close;

  var _ref$overlay = _ref.overlay,
      overlay = _ref$overlay === void 0 ? false : _ref$overlay,
      config = _ref.config;

  var _cookieStore = cookieStore(),
      editCookie = _cookieStore.editCookie,
      dialogVisible = _cookieStore.dialogVisible,
      optionsVisible = _cookieStore.optionsVisible,
      setNewSettings = _cookieStore.setNewSettings,
      setConfig = _cookieStore.setConfig,
      setDialogVisible = _cookieStore.setDialogVisible,
      setActiveCookies = _cookieStore.setActiveCookies,
      setEditCookie = _cookieStore.setEditCookie,
      setOptionsVisible = _cookieStore.setOptionsVisible;

  var _useCookies = useCookies(),
      activeCookies = _useCookies.activeCookies;

  var checkConsentStatus = function checkConsentStatus() {
    var cookieJSON = reactUseCookie.getCookie(config.cookieName);

    if (reactUseCookie.getCookie(config.cookieName)) {
      var cookie = JSON.parse(cookieJSON);
      var newSettingsFound = checkForNewSettings(config.cookies, cookie);

      if (newSettingsFound.length > 0) {
        setNewSettings(newSettingsFound);
        setDialogVisible(true);
        setOptionsVisible(true);
      }

      setActiveCookies(cookie);
    } else {
      setTimeout(function () {
        setDialogVisible(true);
      }, config.banner.delay || 0);
    }
  };

  var onHashChange = function onHashChange() {
    var openSettingsHash = config.openSettingsHash;

    if (openSettingsHash && openSettingsHash === location.hash.substring(1)) {
      history.replaceState({}, document.title, window.location.href.split('#')[0]);
      setDialogVisible(true);
      setOptionsVisible(true);
    }
  };

  React.useEffect(function () {
    if (!activeCookies) {
      return;
    }

    Object.keys(activeCookies).forEach(function (key) {
      if (activeCookies[key] === false) {
        return;
      }

      var script = document.querySelector("script[data-service-handle=\"" + key + "\"]");

      if (!script || script.type !== 'text/plain') {
        return;
      }

      var updatedScript = script.cloneNode(true);

      if (updatedScript && script) {
        var _script$parentNode;

        updatedScript.type = 'text/javascript';
        (_script$parentNode = script.parentNode) === null || _script$parentNode === void 0 ? void 0 : _script$parentNode.replaceChild(updatedScript, script);
      }
    });
  }, [activeCookies]);
  React.useEffect(function () {
    setConfig(config);
    checkConsentStatus();
    window.addEventListener('hashchange', function () {
      onHashChange();
    });
    return window.removeEventListener('hashchange', onHashChange);
  }, []);

  var checkForNewSettings = function checkForNewSettings(cookieConfig, cookie) {
    var cookieList = cookieConfig.optional.map(function (group) {
      return group.options;
    }).flat();
    return cookieList.map(function (v) {
      if (v.handle in cookie) {
        return null;
      } else {
        return v;
      }
    }).filter(function (v) {
      return v;
    });
  };

  React.useEffect(function () {
    if (!editCookie) {
      return;
    }

    setDialogVisible(true);
    setOptionsVisible(true);
  }, [editCookie]);

  var onAccept = function onAccept() {
    var cookieSettings = {};
    var cookiesList = config.cookies.optional.map(function (group) {
      return group.options;
    }).flat();
    cookiesList.map(function (_ref2) {
      var handle = _ref2.handle;
      return cookieSettings[handle] = true;
    });
    saveCookie(cookieSettings, config.cookieName);
    setActiveCookies(cookieSettings);
    setDialogVisible(false);
    setEditCookie(null);
  };

  var onDecline = function onDecline() {
    var cookieSettings = getDeactivatedCookieList(config);
    saveCookie(cookieSettings, config.cookieName);
    setDialogVisible(false);
    setActiveCookies(cookieSettings);
    setEditCookie(null);
  };

  var onAbort = function onAbort() {
    setDialogVisible(false);
    setEditCookie(null);
  };

  if (!dialogVisible) {
    return null;
  }

  var buttons = function buttons() {
    var _buttonProps$openSett, _buttonProps$openSett2, _buttonProps$decline, _buttonProps$decline2, _buttonProps$accept, _buttonProps$accept2;

    var buttonProps = config.banner.buttons;
    return React.createElement("div", {
      className: styles['cookie-banner__buttons'] + " " + config.banner.cButtonWrapper
    }, React.createElement("button", {
      className: "\n            " + styles.button + "\n\n            " + ((buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$openSett = buttonProps.openSettings) === null || _buttonProps$openSett === void 0 ? void 0 : _buttonProps$openSett.classes) || styles['default-button']),
      onClick: function onClick() {
        setOptionsVisible(true);
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$openSett2 = buttonProps.openSettings) === null || _buttonProps$openSett2 === void 0 ? void 0 : _buttonProps$openSett2.label), React.createElement("button", {
      className: "\n          " + styles.button + "\n\n          " + ((buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$decline = buttonProps.decline) === null || _buttonProps$decline === void 0 ? void 0 : _buttonProps$decline.classes) || styles['default-button']),
      onClick: function onClick() {
        onDecline();
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$decline2 = buttonProps.decline) === null || _buttonProps$decline2 === void 0 ? void 0 : _buttonProps$decline2.label), React.createElement("button", {
      className: "\n          " + styles.button + "\n          " + ((buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$accept = buttonProps.accept) === null || _buttonProps$accept === void 0 ? void 0 : _buttonProps$accept.classes) || styles['default-button']),
      onClick: function onClick() {
        onAccept();
      }
    }, buttonProps === null || buttonProps === void 0 ? void 0 : (_buttonProps$accept2 = buttonProps.accept) === null || _buttonProps$accept2 === void 0 ? void 0 : _buttonProps$accept2.label));
  };

  var banner = config.banner;
  var bannerJSX = React.createElement("div", {
    className: "\n        " + styles['cookie-banner'] + "\n        " + banner.className + "\n        " + (optionsVisible && styles['cookie-banner--options-visible'] + " " + banner.optionModeClassName + " ") + "\n    "
  }, React.createElement("div", {
    className: styles.content + " " + ((_config$banner = config.banner) !== null && _config$banner !== void 0 && _config$banner.cContent ? config.banner.cContent : ''),
    style: {}
  }, optionsVisible ? React.createElement(CookieOptionsForm, {
    onAbort: onAbort,
    onDecline: onDecline,
    cookies: config.cookies,
    banner: banner
  }) : banner.content), !optionsVisible && buttons(), React.createElement("button", {
    className: "\n          " + styles.button + "\n\n          " + (((_config$banner$button = config.banner.buttons) === null || _config$banner$button === void 0 ? void 0 : (_config$banner$button2 = _config$banner$button.closeButton) === null || _config$banner$button2 === void 0 ? void 0 : _config$banner$button2.classes) || styles['close-button']),
    onClick: function onClick() {
      setDialogVisible(false);
    }
  }, ((_banner$buttons = banner.buttons) === null || _banner$buttons === void 0 ? void 0 : (_banner$buttons$close = _banner$buttons.closeButton) === null || _banner$buttons$close === void 0 ? void 0 : _banner$buttons$close.label) || 'X'), banner.footer && !optionsVisible && React.createElement("div", {
    className: banner.cFfooter || styles['footer']
  }, banner.footer));

  if (overlay) {
    return React.createElement("div", {
      className: "\n          " + (config.banner.cOverlay ? config.banner.cOverlay : '') + "  " + styles['overlay'] + "\n        "
    }, bannerJSX);
  }

  return React.createElement(React.Fragment, null, " ", bannerJSX, " ");
};

exports.ConsentWrapper = ConsentWrapper;
exports.CookieDialog = CookieDialog;
exports.CookieTable = CookieTable;
exports.useActivateService = useActivateService;
exports.useCookies = useCookies;
//# sourceMappingURL=index.js.map
