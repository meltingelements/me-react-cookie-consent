# me-react-cookie-consent

> Create and manage individual cookie consent option groups

[![NPM](https://img.shields.io/npm/v/me-react-cookie-consent.svg)](https://www.npmjs.com/package/me-react-cookie-consent) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save me-react-cookie-consent
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'me-react-cookie-consent'
import 'me-react-cookie-consent/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
