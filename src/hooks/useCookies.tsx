import { cookieStore } from '../context'

export const useCookies = () => {
  const { setMessage, setEditCookie, setDialogVisible, activeCookies, config } =
    cookieStore()

  const checkConsent = (handle: string) => {
    if (!activeCookies) {
      return false
    }

    return activeCookies[handle] || false
  }

  return {
    config,
    setMessage,
    setEditCookie,
    setDialogVisible,
    activeCookies,
    checkConsent
  }
}
