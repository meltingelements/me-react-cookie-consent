import React, { FunctionComponent, useContext, useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useStore } from 'zustand'
import { cookieStore } from '../context'
import { CookieOption, OptionGroup } from '../types'
import styles from './../me-react-cookie-consent.module.css'
import { Options } from './options'

export const OptionsSingleGroup: FunctionComponent<{
  group: OptionGroup
  mandatory: boolean
  mandatoryLabel: string | JSX.Element
  hasSelectedOptions: boolean
  hasUnselectedOptions: boolean
  index: number
}> = ({
  group,
  mandatory,
  mandatoryLabel = 'mandatory',
  hasSelectedOptions,
  hasUnselectedOptions,
  index
}) => {
  const [isDirty, setIsDirty] = useState(false)
  const { setValue } = useFormContext()
  const { config } = cookieStore()
  const toggleChildOptions = (options: CookieOption[], value: number) => {
    options.forEach((option) => {
      setValue('optional.' + option.handle, value)
    })
  }

  const groupHasIntermediateStatus = hasSelectedOptions && hasUnselectedOptions
  const groupHasCheckedStatus =
    (hasSelectedOptions && !hasUnselectedOptions) || mandatory

  // unique key
  const key = `option-group-control-${mandatory ? '-mandatory-' : ''}${index}`
  return (
    <div
      className={`${styles['option-group']} ${config?.banner?.cOptionGroup}`}
      key={key}
    >
      <div
        className={`${styles['option-group__header']} ${config?.banner?.cOptionGroupHeader}`}
      >
        <div
          className={`${styles[`option-group__toggler`]}
          ${
            config?.banner?.cOptionGroupToggler
              ? config.banner.cOptionGroupToggler
              : ''
          } ${
            groupHasIntermediateStatus && !isDirty
              ? styles['option-group__toggler--intermediate']
              : ''
          } ${mandatory ? styles['option-group__toggler--hidden'] : ''}`}
        >
          <input
            className={styles.input}
            name={key}
            type='checkbox'
            id={key}
            defaultChecked={groupHasCheckedStatus}
            defaultValue={groupHasCheckedStatus ? 1 : 0}
            disabled={!!mandatory}
            onChange={(data) => {
              setIsDirty(true)
              toggleChildOptions(group.options, data.target.checked ? 1 : 0)
            }}
          />
          <label className={styles.label} htmlFor={key} />
          {mandatory && (
            <div
              className={`mandatory-label      ${
                config?.banner?.cOptionGroupTogglerMandatory
                  ? config?.banner?.cOptionGroupTogglerMandatory
                  : ''
              }`}
            >
              <small>{mandatoryLabel}</small>
            </div>
          )}
        </div>
        <div className='option-group__description'>
          {group.headline}
          {group.description}
        </div>
      </div>
      <div className={styles['option-group__options']}>
        <fieldset disabled>
          <Options options={group.options} mandatory={mandatory} />
        </fieldset>
      </div>
    </div>
  )
}
