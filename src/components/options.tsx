import React, { FunctionComponent } from 'react'
import { useFormContext } from 'react-hook-form'
import { cookieStore } from '../context'
import { CookieOption } from '../types'
import styles from './../me-react-cookie-consent.module.css'

interface IOptionsProps {
  options: CookieOption[]
  mandatory: boolean
}

export const Options: FunctionComponent<IOptionsProps> = ({
  options,
  mandatory = false
}) => {
  const { register } = useFormContext()
  const { activeCookies } = cookieStore()

  return (
    <React.Fragment>
      {options.map((v, i) => {
        const savedValue = activeCookies ? activeCookies[v.handle] : undefined
        const isChecked = savedValue !== undefined ? savedValue : mandatory

        return (
          <label
            key={`co-${i}-${v.handle}`}
            className={`${styles['cookie-option']} ${styles.label}`}
          >
            <input
              className={styles['cookie-option__checkbox']}
              type='checkbox'
              defaultChecked={isChecked}
              defaultValue={isChecked ? 1 : 0}
              {...register(
                `${mandatory ? 'mandatory.' : 'optional.'}${v.handle}`
              )}
            />
            <div className={styles['cookie-option__description']}>
              {v.label}
            </div>
          </label>
        )
      })}
    </React.Fragment>
  )
}
