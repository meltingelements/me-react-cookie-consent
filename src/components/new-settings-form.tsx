import React, { FunctionComponent, useContext } from 'react'
import { FieldValues, useForm } from 'react-hook-form'
import { cookieStore } from '../context'
import { saveCookie } from '../helper/saveCookie'
import { CookieOption, ICookieBannerProps } from '../types'
import styles from './../me-react-cookie-consent.module.css'

interface INewSettingsFormProps {
  banner: ICookieBannerProps
}

export const NewSettingsForm: FunctionComponent<INewSettingsFormProps> = (
  props
) => {
  const methods = useForm()
  const {
    newSettings,
    activeCookies,
    setActiveCookies,
    config,
    setDialogVisible,
    setOptionsVisible,
    setNewSettings,
    setMessage
  } = cookieStore()

  const onFormSubmit = (values: FieldValues): void => {
    const updateCookie = { ...activeCookies, ...values }
    // save cookie
    saveCookie(updateCookie, config?.cookieName || '')
    // save to local cookie state
    setActiveCookies(updateCookie)

    // hide settings and reset ui
    setDialogVisible(false)
    setOptionsVisible(false)
    setNewSettings(null)
    setMessage(null)
  }

  const getNewSettings = (
    cookies: Array<CookieOption | null>
  ): JSX.Element[] => {
    const settings = cookies.map((v, i) => {
      const key = `additional-cookie-checkbox--${i}`

      if (!v) {
        return <React.Fragment></React.Fragment>
      }

      return (
        <div className={styles['cookie-option']} key={key}>
          <input
            type='checkbox'
            className={styles['cookie-option__checkbox']}
            id={key}
            {...methods.register(v.handle)}
          ></input>
          <label className={styles.label}htmlFor={key}>{v.label}</label>
        </div>
      )
    })

    return settings
  }
  return (
    <form
      className={styles['option-group']}
      onSubmit={methods.handleSubmit(onFormSubmit)}
    >
      {newSettings && getNewSettings(newSettings)}
      <button
        className={
          props.banner?.buttons?.acceptSettings?.classes ||
          styles['default-button']
        }
        type='submit'
      >
        {props.banner?.buttons?.acceptSettings?.label || 'Confirm Settings'}
      </button>
    </form>
  )
}
