import React, { FunctionComponent } from 'react'
import { cookieStore } from '../context'
import { CookieOption, OptionGroup } from '../types'
import { OptionsSingleGroup } from './options-single-group'

interface IOptionGroupsProps {
  groups: OptionGroup[]
  mandatory: boolean
  mandatoryLabel?: string | JSX.Element
}

export const OptionsGroups: FunctionComponent<IOptionGroupsProps> = ({
  groups,
  mandatory,
  mandatoryLabel = 'mandatory'
}) => {
  const { activeCookies } = cookieStore()

  /**
   * check wether the group should be preselected for the user
   * @param group
   */

  const checkIfGroupPreselected = (group: CookieOption[]) => {
    // check if options group was already selected
    const hasSelected = group.some(({ handle }) => activeCookies?.[handle])
    const hasUnselected = group.some(
      ({ handle }) =>
        activeCookies?.[handle] === undefined ||
        activeCookies?.[handle] === false
    )

    return { hasSelected, hasUnselected }
  }

  return (
    <React.Fragment>
      {groups.map((group, i) => {
        const { hasSelected, hasUnselected } = checkIfGroupPreselected(
          group.options
        )

        return (
          <OptionsSingleGroup
            group={group}
            mandatory={mandatory}
            mandatoryLabel={mandatoryLabel}
            hasSelectedOptions={hasSelected}
            hasUnselectedOptions={hasUnselected}
            index={i}
          ></OptionsSingleGroup>
        )
      })}
    </React.Fragment>
  )
}
