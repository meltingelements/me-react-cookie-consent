import React, { FunctionComponent } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { cookieStore } from '../context'
import { saveCookie } from '../helper/saveCookie'
import { CookieData, ICookieBannerProps, ICookiesProps } from '../types'
import styles from './../me-react-cookie-consent.module.css'
import { NewSettingsForm } from './new-settings-form'
import { OptionsGroups } from './options-groups'

interface ICookieOptionsForm {
  onDecline: () => void
  onAbort: () => void
  onDialogSubmit?: (cookieList: {}) => void
  cookies: ICookiesProps
  banner: ICookieBannerProps
}

export const CookieOptionsForm: FunctionComponent<ICookieOptionsForm> = (
  props
) => {
  const {
    cookies,
    onDecline,
    onAbort,
    banner: { buttons, cOptionsForm = undefined }
  } = props
  const methods = useForm()

  const {
    config,
    editCookie,
    newSettings,
    setDialogVisible,
    setActiveCookies,
    setEditCookie,
    message,
    setMessage
  } = cookieStore()

  const onFormSubmit = (cookieValues: CookieData | any = {}) => {
    const optionalCookieValues = { ...cookieValues.optional }
    const updateValues = {}

    Object.keys(optionalCookieValues).forEach(
      (key) => (updateValues[key] = !!optionalCookieValues[key] ? true : false)
    )

    saveCookie(updateValues, config?.cookieName || '')

    // #1 - set the cookies
    setActiveCookies && setActiveCookies(updateValues)
    // #2 - fire optional external callback
    props.onDialogSubmit && props.onDialogSubmit(updateValues)
    // #3 close the dialog
    setDialogVisible(false)
    setEditCookie(null)
    setMessage(null)
  }

  const getMessage = (message: string | React.ReactNode) => {
    return <div className={styles['message']}>{message}</div>
  }

  return (
    <FormProvider {...methods}>
      {message && getMessage(message || '')}
      {newSettings ? (
        <React.Fragment>
          {getMessage(config?.banner.newCookieFoundMessage || '')}
          <NewSettingsForm banner={props.banner}></NewSettingsForm>
        </React.Fragment>
      ) : (
        <form
          onSubmit={methods.handleSubmit(onFormSubmit)}
          className={`${styles.options} ${cOptionsForm ? cOptionsForm : ''}`}
        >
          <OptionsGroups
            groups={cookies.mandatory}
            mandatory={true}
            mandatoryLabel={config?.banner.mandatoryLabel}
          />
          <OptionsGroups groups={cookies.optional} mandatory={false} />
          <button className={buttons?.acceptSettings?.classes} type='submit'>
            {buttons?.acceptSettings?.label || 'Confirm Settings'}
          </button>
          <button
            className={buttons?.closeSettings?.classes}
            onClick={editCookie ? onDecline : onAbort}
          >
            {buttons?.closeSettings?.label || 'Cancel'}
          </button>
        </form>
      )}
    </FormProvider>
  )
}
