import React, { FunctionComponent, useMemo } from 'react'
import { useCookies } from '../hooks/useCookies'

export const ConsentWrapper: FunctionComponent<{
  consent: React.ReactElement
  default: React.ReactElement
  handle: string
}> = (props) => {
  const { checkConsent, activeCookies } = useCookies()

  const cookieAccepted = useMemo(
    () => checkConsent(props.handle),
    [activeCookies, props.handle]
  )

  return (
    <React.Fragment>
      {cookieAccepted ? props.consent : props.default}
    </React.Fragment>
  )
}
