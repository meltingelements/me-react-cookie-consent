import * as React from 'react'
import { useEffect } from 'react'
import { getCookie } from 'react-use-cookie'
import { ConsentWrapper } from './components/consent-wrapper'
import { CookieOptionsForm } from './components/options-form'
import { cookieStore } from './context'
import { useActivateService } from './helper/activateService'
import { getDeactivatedCookieList } from './helper/getDeactivatedCookieList'
import { saveCookie } from './helper/saveCookie'
import { useCookies } from './hooks/useCookies'
import styles from './me-react-cookie-consent.module.css'
import { CookieTable } from './table'
import {
  CookieOption,
  ICookieBannerProps,
  ICookieConfig,
  ICookiesProps,
  MeCookieConsentProps,
  OptionGroup
} from './types'

const CookieDialog: React.FunctionComponent<MeCookieConsentProps> = ({
  overlay = false,
  config
}) => {
  const {
    editCookie,
    dialogVisible,
    optionsVisible,
    setNewSettings,
    setConfig,
    setDialogVisible,
    setActiveCookies,
    setEditCookie,
    setOptionsVisible
  } = cookieStore()
  const { activeCookies } = useCookies()

  const checkConsentStatus = () => {
    const cookieJSON = getCookie(config!.cookieName)
    if (getCookie(config!.cookieName)) {
      const cookie = JSON.parse(cookieJSON)
      const newSettingsFound = checkForNewSettings(config!.cookies, cookie)

      if (newSettingsFound.length > 0) {
        setNewSettings(newSettingsFound)
        setDialogVisible(true)
        setOptionsVisible(true)
      }

      setActiveCookies!(cookie)
    } else {
      setTimeout(() => {
        setDialogVisible!(true)
      }, config!.banner.delay || 0)
    }
  }

  const onHashChange = () => {
    const { openSettingsHash } = config

    if (openSettingsHash && openSettingsHash === location.hash.substring(1)) {
      // REMOVE HASH
      history.replaceState(
        {},
        document.title,
        window.location.href.split('#')[0]
      )
      setDialogVisible(true)
      setOptionsVisible(true)
    }
  }

  /**
   * Handle script-tags after the cookie settings changed
   */
  useEffect(() => {
    if (!activeCookies) {
      return
    }

    Object.keys(activeCookies).forEach((key) => {
      if (activeCookies[key] === false) {
        return
      }
      /**  Removing "text/plain" is not enough to execute a script. Script Tag must be cloned and appended to the DOM in order to get executed */
      const script = document.querySelector(
        `script[data-service-handle="${key}"]`
      ) as HTMLScriptElement | null

      if (!script || script.type !== 'text/plain') {
        return
      }

      let updatedScript = script.cloneNode(true) as HTMLScriptElement

      if (updatedScript && script) {
        updatedScript.type = 'text/javascript'
        script.parentNode?.replaceChild(updatedScript, script)
      }
    })
  }, [activeCookies])

  /**
   * init cookie settings
   */

  useEffect(() => {
    setConfig(config)
    checkConsentStatus()
    window.addEventListener('hashchange', () => {
      onHashChange()
    })

    return window.removeEventListener('hashchange', onHashChange)
  }, [])

  const checkForNewSettings = (
    cookieConfig: ICookiesProps,
    cookie: any
  ): Array<CookieOption | null> => {
    const cookieList = cookieConfig.optional
      .map((group) => group.options)
      .flat()

    return cookieList
      .map((v) => {
        if (v.handle in cookie) {
          return null
        } else {
          return v
        }
      })
      .filter((v) => v)
  }

  useEffect(() => {
    if (!editCookie) {
      return
    }

    setDialogVisible!(true)
    setOptionsVisible!(true)
  }, [editCookie])

  /**
   * On accept all button click
   */
  const onAccept = () => {
    const cookieSettings = {}
    const cookiesList = config.cookies.optional
      .map((group) => group.options)
      .flat()
    cookiesList.map(({ handle }) => (cookieSettings[handle] = true))
    saveCookie(cookieSettings, config.cookieName)
    setActiveCookies!(cookieSettings)
    setDialogVisible!(false)
    setEditCookie(null)
  }

  /**
   * On decline all button click
   */
  const onDecline = () => {
    const cookieSettings = getDeactivatedCookieList(config)
    saveCookie(cookieSettings, config.cookieName)
    setDialogVisible!(false)
    setActiveCookies!(cookieSettings)
    setEditCookie(null)
  }

  /**
   * On decline all button click
   */
  const onAbort = () => {
    setDialogVisible!(false)
    setEditCookie(null)
  }

  if (!dialogVisible) {
    return null
  }

  /**
   * get the cookie "accept" and the "decline button"
   * @returns React.ReactNode
   */
  const buttons = () => {
    const buttonProps = config.banner.buttons
    return (
      <div
        className={`${styles['cookie-banner__buttons']} ${config.banner.cButtonWrapper}`}
      >
        <button
          className={`
            ${styles.button}

            ${buttonProps?.openSettings?.classes || styles['default-button']}`}
          onClick={() => {
            setOptionsVisible!(true)
          }}
        >
          {buttonProps?.openSettings?.label}
        </button>

        <button
          className={`
          ${styles.button}

          ${buttonProps?.decline?.classes || styles['default-button']}`}
          onClick={() => {
            onDecline()
          }}
        >
          {buttonProps?.decline?.label}
        </button>

        <button
          className={`
          ${styles.button}
          ${buttonProps?.accept?.classes || styles['default-button']}`}
          onClick={() => {
            onAccept()
          }}
        >
          {buttonProps?.accept?.label}
        </button>
      </div>
    )
  }

  const { banner } = config

  const bannerJSX = (
    <div
      className={`
        ${styles['cookie-banner']}
        ${banner.className}
        ${
          optionsVisible &&
          `${styles['cookie-banner--options-visible']} ${banner.optionModeClassName} `
        }
    `}
    >
      <div
        className={`${styles.content} ${
          config.banner?.cContent ? config.banner.cContent : ''
        }`}
        style={
          {
            // maxWidth: `${banner.contentMaxWidth + 'px' || 'none'}`
          } as React.CSSProperties
        }
      >
        {optionsVisible ? (
          <CookieOptionsForm
            onAbort={onAbort}
            onDecline={onDecline}
            cookies={config.cookies}
            banner={banner}
          ></CookieOptionsForm>
        ) : (
          banner.content
        )}
      </div>
      {!optionsVisible && buttons()}

      <button
        className={`
          ${styles.button}

          ${
            config.banner.buttons?.closeButton?.classes ||
            styles['close-button']
          }`}
        onClick={() => {
          setDialogVisible!(false)
        }}
      >
        {banner.buttons?.closeButton?.label || 'X'}
      </button>
      {banner.footer && !optionsVisible && (
        <div className={banner.cFfooter || styles['footer']}>
          {banner.footer}
        </div>
      )}
    </div>
  )

  if (overlay) {
    return (
      <div
        className={`
          ${config.banner.cOverlay ? config.banner.cOverlay: ''}  ${styles['overlay']}
        `}
      >
        {bannerJSX}
      </div>
    )
  }

  return <React.Fragment> {bannerJSX} </React.Fragment>
}
export {
  ConsentWrapper,
  CookieDialog,
  CookieTable,
  CookieOption,
  useCookies,
  MeCookieConsentProps,
  ICookieBannerProps,
  ICookieConfig,
  OptionGroup,
  ICookiesProps,
  useActivateService
}
