import React from 'react'
import { FunctionComponent } from 'react'
import { CookieOption, ICookiesProps, OptionGroup } from './types'
import style from './table.module.css'
import { uid } from 'react-uid'

export interface ICookieTable {
  onlyMandatory?: boolean
  onlyOptional?: boolean
  labelCookieDuration?: string | JSX.Element
  labelCookieDescription?: string | JSX.Element
  labelCookieName?: string | JSX.Element
  cTable?: string
  cHead?: string
  cBody?: string
  groupHandles?: string[]
  cookies: ICookiesProps
}

export const CookieTable: FunctionComponent<ICookieTable> = ({
  cookies: { optional, mandatory },
  onlyMandatory = false,
  onlyOptional = false,
  labelCookieDuration = 'Valid for',
  labelCookieDescription = 'Description',
  labelCookieName = 'Name',
  ...props
}) => {
  const tableHeader: JSX.Element = (
    <thead className={`${style.head} ${props.cHead}`}>
      <tr>
        <td>{labelCookieName}</td>
        <td>{labelCookieDescription}</td>
        <td>{labelCookieDuration}</td>
      </tr>
    </thead>
  )

  const renderRow = (cookies: CookieOption[]) => {
    return (
      <React.Fragment key={uid(cookies)}>
        {cookies.map(
          ({ duration = '', name = '', description = '' }, i: number) => (
            <tr key={uid(i)}>
              <td>{name}</td>
              <td>{description}</td>
              <td>{duration}</td>
            </tr>
          )
        )}
      </React.Fragment>
    )
  }

  const renderTable = (optionGroups: OptionGroup[]): JSX.Element => {
    return (
      <table className={`${style.table} ${props.cTable}`}>
        {tableHeader}
        <tbody className={`${style.body} ${props.cBody}`}>
          {optionGroups.map(({ options }) => renderRow(options))}
        </tbody>
      </table>
    )
  }

  const filterOptions = (optionGroups: OptionGroup[], handles: string[]) => {

     return optionGroups.filter((v) => handles.includes(v.handle || '' ))

  }

  return (
    <React.Fragment>
      {!onlyOptional && renderTable(mandatory)}
      {!onlyMandatory &&
        renderTable(props.groupHandles ? filterOptions(optional, props.groupHandles) : optional)}
    </React.Fragment>
  )
}
