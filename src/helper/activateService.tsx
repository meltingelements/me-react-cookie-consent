import { CookieOption } from 'me-react-cookie-consent'
import { cookieStore } from '../context'
import { getDeactivatedCookieList } from './getDeactivatedCookieList'
import { saveCookie } from './saveCookie'

export const useActivateService = () => {
  const { config, activeCookies, setActiveCookies } = cookieStore()

  const activate = (cookieHandle: CookieOption['handle']) => {
    if (!config?.cookieName) {
      console.log('error: cannot activate service - no cookiename provided')
      return
    }

    const servicesList = activeCookies
      ? activeCookies
      : getDeactivatedCookieList(config)

    const services = { ...servicesList, [cookieHandle]: true }

    saveCookie(services, config.cookieName)
    setActiveCookies(services)
  }

  return activate
}
