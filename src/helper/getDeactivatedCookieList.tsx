import { ICookieConfig } from 'me-react-cookie-consent'

export const getDeactivatedCookieList = (config: ICookieConfig) => {
  const cookieSettings = {}
  config.cookies.optional
    .map((group) => group.options)
    .flat()
    .map(({ handle }) => (cookieSettings[handle] = false))

  return cookieSettings
}
