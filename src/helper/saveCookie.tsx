import { setCookie } from "react-use-cookie"

export const saveCookie = (
  settings: { [key: string]: boolean } | undefined,
  name: string
): void => {
  const payload = JSON.stringify(settings)
  setCookie(name || 'MECC_TECHNICAL', payload, {
    path: '/',
    days: 365
  })
}
