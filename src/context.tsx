import * as React from 'react'
import {
  CookieData,
  CookieOption,
  MeCookieConsentProps
} from './types'
import create from 'zustand'

interface CookieState {
  activeCookies: CookieData | undefined | null
  editCookie: CookieOption['handle'] | undefined | null
  optionsVisible: boolean
  dialogVisible: boolean
  config: MeCookieConsentProps['config'] | undefined
  newSettings: Array<CookieOption | null> | undefined | null
  message: string | React.ReactNode | null | undefined
  setActiveCookies: (cookie: CookieData) => void
  setEditCookie: (handle: CookieOption['handle'] | null) => void
  setOptionsVisible: (visible: boolean) => void
  setDialogVisible: (visible: boolean) => void
  setNewSettings: (settings: Array<CookieOption | null> | null) => void
  setConfig: (config: MeCookieConsentProps['config']) => void
  setMessage: (message: string | React.ReactNode | null | undefined) => void
}

export const cookieStore = create<CookieState>((set) => ({
  activeCookies: undefined,
  editCookie: undefined,
  optionsVisible: false,
  dialogVisible: false,
  newSettings: undefined,
  config: undefined,
  message: undefined,
  setConfig: (config: MeCookieConsentProps['config']) =>
    set(() => ({ config })),
  setActiveCookies: (cookies: CookieData) =>
    set(() => ({ activeCookies: cookies })),
  setEditCookie: (handle: CookieOption['handle'] | null) =>
    set(() => ({ editCookie: handle })),
  setOptionsVisible: (visible: boolean) =>
    set(() => ({ optionsVisible: visible })),
  setDialogVisible: (visible: boolean) =>
    set(() => ({ dialogVisible: visible })),
  setNewSettings: (setting: Array<CookieOption> | null) =>
    set(() => ({ newSettings: setting })),
  setMessage: (setting: string | React.ReactNode | null) =>
    set(() => ({ message: setting }))
}))
