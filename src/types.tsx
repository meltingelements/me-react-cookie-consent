export interface OptionGroup {
  handle?: string
  headline?: React.ReactNode | string
  description?: React.ReactNode | string
  validity?: React.ReactNode | string
  options: CookieOption[]
}

export interface CookieOption {
  handle: string
  label: string | React.ReactNode
  description?: string | React.ReactNode
  duration?: string | React.ReactNode
  name?: string | React.ReactNode
  scriptTag?: HTMLScriptElement | undefined
}
export interface buttonProps {
  hide?: boolean
  label?: string | React.ReactNode
  color?: string
  classes?: string | undefined
}
/**
 *
 */
export interface ICookieBannerProps {
  //time in MS before first appearance of the banner
  delay?: number
  className?: string
  optionModeClassName?: string
  innerMaxWidth?: string
  content?: React.ReactNode | string
  contentMaxWidth?: string
  buttons?: {
    accept?: buttonProps
    decline?: buttonProps
    openSettings?: buttonProps
    acceptSettings?: buttonProps
    closeSettings?: buttonProps
    closeButton?: buttonProps
  }
  footer?: React.ReactNode | string,
  cContent?: string,
  cButtonWrapper?: string,
  cOverlay?: string,
  cFfooter?: string,
  cMessage?: string,
  cOptionGroup?: string,
  cOptionGroupHeader?: string,
  cOptionGroupToggler?: string,
  cOptionGroupTogglerMandatory?: string,
  cOverlayBanner?: string,
  cOptionsForm?: string
  newCookieFoundMessage?: string | React.ReactNode
  editCookieMessage?: string | React.ReactNode
  mandatoryLabel?: string
}
export interface ICookiesProps {
  mandatory: OptionGroup[]
  optional: OptionGroup[]
}

export interface ICookieConfig {
  banner: ICookieBannerProps
  openSettingsHash?: string,
  cookieName: string
  cookies: ICookiesProps
  onDialogSubmit?: (cookieList: {}) => void
  onActiveCookies?: (cookieList: { [key: string]: boolean } | undefined) => void
}
export interface MeCookieConsentProps {
  config: ICookieConfig
  overlay?:boolean
}
export interface ICookieContext {
  setActiveCookies: (cookies: CookieData | undefined) => void
  setEditCookie: (handle: CookieOption['handle'] | undefined) => void
  activeCookies: CookieData | undefined
  editCookie: CookieOption['handle'] | undefined
  allowedScripts: (React.ReactNode | undefined)[]
  setOptionsVisible: (show: boolean) => void
  optionsVisible: boolean
  setDialogVisible: (show: boolean) => void
  setNewSettings: (options: Array<CookieOption | null> | null) => void
  newSettings: Array<CookieOption | null> | null
  dialogVisible: boolean
  config: MeCookieConsentProps
  saveCookie: (
    cookieSettngs:
      | {
          [key: string]: boolean
        }
      | undefined
  ) => void

  children?: React.ReactNode
}
export interface CookieData {
  [key: string]: boolean
}
