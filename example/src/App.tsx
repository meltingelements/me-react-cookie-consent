import {
  ICookieBannerProps,
  ICookiesProps,
  CookieTable,
  CookieDialog,
  ConsentWrapper,
  useActivateService
} from 'me-react-cookie-consent'
import 'me-react-cookie-consent/dist/index.css'
import { FunctionComponent, useEffect } from 'react'

const banner: ICookieBannerProps = {
  delay: 1000,
  content: (
    <>
      <h3>🍪 Cookie Consent</h3>
      <p>
        Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor!
        Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor -
        hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor
        hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor...
        Hodor hodor hodor...
      </p>
    </>
  ),
  buttons: {
    accept: { label: 'Alle akzeptieren' },
    decline: { label: 'Nur notwendige' },
    openSettings: { label: 'Cookies Konfigurieren' },
    closeSettings: { label: 'schließen' }
  },
  cOverlay: 'TESTCLASS',
  cFfooter: 'TESTCLASS',
  cMessage: 'TESTCLASS',
  cOptionGroup: 'TESTCLASS',
  cOptionGroupHeader: 'TESTCLASS',
  cOptionGroupToggler: 'TESTCLASS',
  cOptionGroupTogglerMandatory: 'TESTCLASS',
  cOverlayBanner: 'TESTCLASS',
  cOptionsForm: 'TESTCLASS',

  newCookieFoundMessage:
    'Wir haben seit der letzten änderungen Weitere Cookies hinzugefügt'
}

const cookies: ICookiesProps = {
  mandatory: [
    {
      headline: <h3>Mandatory Settings</h3>,
      description: (
        <h5>
          Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor.
          Hodor! Hodor hodor, hodor; hodor hodor hodor.w
        </h5>
      ),
      options: [
        {
          handle: 'analytics',
          name:'mandatory cookie 1',
          label: <p>Google maps, hodor</p>,
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },
        {
          handle: 'hotjar',
          name:'mandatory cookie 2',
          label: <p>Google maps, hodor</p>,
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        }
      ]
    }
  ],
  optional: [
    {
      headline: <h3>Optional Settings</h3>,
      handle: 'group1',
      description: (
        <h5>
          Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor.
          Hodor! Hodor hodor, hodor; hodor hodor hodor.
        </h5>
      ),
      options: [
        {
          handle: 'googleMaps',
          label: <p>Video </p>,
          name: 'GMAPS',
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },
        {
          handle: 'vimeo',
          label: <p>Vimeo</p>,
          name: 'GMAPS',
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },
        {
          handle: 'newVimeo',
          label: <p>newVimeo</p>,
          name: 'newVimeo',
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },
        {
          handle: 'youtube',
          label: <p>Youtube</p>,
          name: 'GMAPS',
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },
        {
          handle: 'newCookieTest',
          label: <p>new cookie</p>,
          name: 'NEW COOKIE',
          duration: '2 days',
          description: 'DESCrsatfasjgjsajkgjisakjgasj'
        },

        {
          handle: 'dontAcceptCookie',
          label: <p>I wont accept this</p>,
          name: 'Dont accept COOKIE',
          duration: '2 days',
          description: 'this is not cool'
        }
      ]
    },
    {
      headline: <h3>Optional Settings 2</h3>,
      handle: 'group2',
      description: (
        <h5>
          Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor.
          Hodor! Hodor hodor, hodor; hodor hodor hodor.
        </h5>
      ),
      options: [
        {
          handle: 'googleMaps2',
          label: <p>Google maps, hodor</p>
        },
        // {
        //   handle: 'vimeo2',
        //   label: <p>I am the New Cookie</p>
        // },
        {
          handle: 'vimeo3',
          label: <p>I am the New Cookie</p>,
          duration: '2 days',
          description: 'imherefortesting'
        },
        {
          handle: 'noo',
          label: <p>Google maps, hodor</p>
        },
        {
          handle: 'youtube2',
          label: <p>Google maps, hodor</p>
        },
        {
          handle: 'TriggerDisShit',
          label: <p>Google maps, hodor</p>
        }
      ]
    }
  ]
}

export const config = {
  banner,
  openSettingsHash: 'opencookiesettings',
  cookieName: 'MELTING_TEST_COOKIE',
  cookies
}

const App: FunctionComponent = () => {
  const activateService = useActivateService()

  useEffect(() => {
    const script = document.createElement('script')
    script.textContent = "console.log('yay')"
    script.setAttribute('type', 'text/plain')
    script.dataset.serviceHandle = 'newVimeo'
    document.head.appendChild(script)
  }, [])

  return (
    <>
      <h1>Cookie Time </h1>
      <section>
        <h1>Cookie Table</h1>
        <CookieTable groupHandles={['group1']} cookies={cookies}></CookieTable>
      </section>

      <CookieDialog config={config} />

      <section>
        <h1>Open Settings Hash</h1>
        <p>
          The Cookie Dialog can be triggered by providing a hash inside the
          navigation
        </p>

        <nav>
          <a href='#opencookiesettings'>Open Cookie Settings</a>
        </nav>
      </section>

      <section>
        <h1>Consent Wrapper</h1>
        <p>
          For easy wrapping of Content that is supposed to only render if a
          certain service is activated.
        </p>

        <ConsentWrapper
          consent={<p>Consent given</p>}
          default={
            <p>
              <h2>Consentwrapper - no consent given for "newCookieTest" </h2>
              <button
                onClick={() => {
                  activateService('newCookieTest')
                }}
              >
                Give consent
              </button>
            </p>
          }
          handle={'newCookieTest'}
        ></ConsentWrapper>
      </section>
    </>
  )
}

export default App
